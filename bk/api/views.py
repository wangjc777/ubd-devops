# -*- coding:utf-8 -*-

from bk.utils.ESB import ESBApi
from common.mymako import render_mako_context, render_json


def search_business(request):
    response = {}
    result = []
    value = {}

    try:
        api = ESBApi(request)
        params = api.search_business()
        '''
            {
                "result": true,
                "code": 0,
                "message": "",
                "data": {
                    "count": 1,
                    "info": [
                        {
                            "bk_biz_id": 1,
                            "bk_biz_name": "esb-test"
                        }
                    ]
                }
            }
        '''
        if params["result"] and params["data"]["count"] > 0:
            for item in params["data"]["info"]:
                data = {}
                data["bk_biz_id"] = item["bk_biz_id"]
                data["bk_biz_name"] = item["bk_biz_name"]
                result.append(data)
        value["info"] = result
        value["count"] = params["data"]["count"]
        response["message"] = "success"
        response["code"] = 0
        response["result"] = True
        response["data"] = value
    except Exception as e:
        response["message"] = u'获取业务失败，失败原因%s' % e
        response["code"] = -1
        response["result"] = False
    return render_json(response)


def search_host(request):
    response = {}
    list = []
    data = {}
    try:
        api = ESBApi(request)
        biz_id = int(request.GET.get('bk_biz_id'))
        result = api.search_host(biz_id=biz_id)
        if result['code'] == 0:
            if result['data']['count'] > 0:
                for biz_info in result['data']['info']:
                    listDic = {}
                    listDic['hostname'] = biz_info['host']['bk_host_name']
                    listDic['ip'] = biz_info['host']['bk_host_innerip']
                    listDic['os_type'] = biz_info['host']['bk_os_type']
                    listDic['os_name'] = biz_info['host']['bk_os_name']
                    listDic['host_name'] = biz_info['host']['bk_host_name']
                    bk_cloud = biz_info['host']['bk_cloud_id']
                    listDic['area'] = bk_cloud[0]['bk_inst_name']
                    listDic['area_id'] = bk_cloud[0]['bk_inst_id']
                    list.append(listDic)
            data["list"] = list
            data["count"] = len(list)
            response["data"] = data
            response["message"] = "success"
            response["code"] = 0
            response["result"] = True
    except Exception as e:
        response["message"] = u'获取IP列表失败，失败原因%s' % e
        response["code"] = -1
        response["result"] = False
    return render_json(response)


def get_user(request):
    response = {}
    list = []
    data = {}
    try:
        api = ESBApi(request)
        params = api.get_user()
        data["username"] = params["data"]["bk_username"]
        data["chname"] = params["data"]["chname"]
        data["email"] = params["data"]["email"]
        response["data"] = data
        response["message"] = "success"
        response["code"] = 0
        response["result"] = True
    except Exception as e:
        response["message"] = u'获取用户失败，失败原因%s' % e
        response["code"] = -1
        response["result"] = False
    return render_json(response)
