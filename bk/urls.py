# -*- coding: utf-8 -*-

from django.conf.urls import patterns

urlpatterns = patterns(
    'bk.api.views',
    (r'^business$', 'search_business'),
    (r'^user$', 'get_user'),
    (r'^host$', 'search_host'),
    # (r'^getAllJobs/$', 'get_all_jobs'),
    # (r'^executeJob/$', 'execute_job'),
    # (r'^getJobInstanceLog/$', 'get_job_instance_log'),
    # (r'^getJobInstanceStatus/$', 'get_job_instance_status'),
)
