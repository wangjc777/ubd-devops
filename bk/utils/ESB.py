# -*- coding: utf-8 -*-


from conf.default import APP_ID, APP_TOKEN
from django.http.request import HttpRequest
from blueking.component.shortcuts import get_client_by_request, get_client_by_user
from blueking.component.client import ComponentClient


class ESBApi(object):

    def __init__(self, param):
        if isinstance(param, HttpRequest):
            self.__client = get_client_by_request(param)  # 获取用户登陆态
            self.username = param.user.username
        else:
            self.__client = get_client_by_user(param)  # 获取到用户对象
            self.username = param
        self.__param = {
            "app_code": APP_ID,
            "app_secret": APP_TOKEN,
            'bk_username': self.username
        }

    def get_app_by_user(self):
        try:
            param = self.__param
            self.__client.set_bk_api_ver('')
            result = self.__client.cc.get_app_by_user(param)
        except Exception, e:
            result = {'message': e}

        return result

    def get_all_users(self):
        try:
            param = self.__param
            result = self.__client.bk_login.get_all_users(param)
        except Exception, e:
            result = {'message': e}

        return result

    def get_user(self):
        try:
            param = self.__param
            result = self.__client.bk_login.get_user(param)
        except Exception,e:
            result = {'message': e}
        return result


    def search_host(self, biz_id, page=None):
        try:
            if page is None:
                page = {"start": 0, "limit": 200}
            param = self.__param
            param['page'] = page
            param['condition'] = [
                {
                    "bk_obj_id": "biz",
                    "fields": [
                        "default",
                        "bk_biz_id",
                        "bk_biz_name",
                    ],
                    "condition": [
                        {
                            "field": "bk_biz_id",
                            "operator": "$eq",
                            "value": biz_id
                        }
                    ]
                }
            ]
            result = self.__client.cc.search_host(param)
        except Exception, e:
            result = {'message': e}

        return result

    def search_business(self, page=None):
        try:
            if page is None:
                page = {"start": 0, "limit": 20, "sort": 'bk_host_id'}
            param = self.__param
            param['page'] = page
            result = self.__client.cc.search_business(param)
        except Exception, e:
            result = {'message': e}

        return result
